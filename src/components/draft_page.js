import React, {Component} from 'react';

import 'styles/index.css';

import Draft from 'components/draft/draft.js'

class DraftPage extends Component {

  render() {
    return (
      <div className="app">
        <Draft />
      </div>
    );
  }
}

export default DraftPage;
