import React, {Component} from 'react';

import 'styles/draft/draft.css';

import BanBlock from 'components/draft/ban_block.js'
import ChampionBlock from 'components/draft/champion_block.js'
import InfoBlock from 'components/draft/info_block.js'
import PlayerBlock from 'components/draft/player_block.js'

const serverUrl = "https://server-dot-instalock.wl.r.appspot.com";
const draftStates = [
  createDraftState("BAN", "blue", 1),
  createDraftState("BAN", "blue", 2),
  createDraftState("BAN", "blue", 3),
  createDraftState("BAN", "blue", 4),
  createDraftState("BAN", "blue", 5),
  createDraftState("BAN", "red", 1),
  createDraftState("BAN", "red", 2),
  createDraftState("BAN", "red", 3),
  createDraftState("BAN", "red", 4),
  createDraftState("BAN", "red", 5),
  createDraftState("PICK", "blue", 1),
  createDraftState("PICK", "red", 1),
  createDraftState("PICK", "red", 2),
  createDraftState("PICK", "blue", 2),
  createDraftState("PICK", "blue", 3),
  createDraftState("PICK", "red", 3),
  createDraftState("PICK", "red", 4),
  createDraftState("PICK", "blue", 4),
  createDraftState("PICK", "blue", 5),
  createDraftState("PICK", "red", 5),
  createDraftState("complete", null, null),
]
const normalDraftOrder = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];
const tournamentDraftOrder = [
    0, 5, 1, 6, 2, 7, 10, 11, 12, 13, 14, 15, 8, 3, 9, 4, 16, 17, 18, 19, 20];

function createDraftState(phase, activeTeam, activePlayer) {
  let status = "Draft Complete";
  if (phase !== "complete") {
    const phaseText = phase === "BAN" ? "Ban" : "Pick";
    status = capitalize(activeTeam) + ": " + phaseText + " " + activePlayer;
  }
  return {
    "phase": phase,
    "activeTeam": activeTeam,
    "activePlayer": activePlayer,
    "status": status,
  }
}

function capitalize(str) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}

class Draft extends Component {

  constructor(props) {
    super(props);
    this.state = {
      currentStep: 0,
      draftType: "NORMAL", // [NORMAL|TOURNAMENT]
      activeHover: null,
      championScoreData: null,
      championRoles: null,
      championScores: null,
      picks: {
        blue: [
          null,
          null,
          null,
          null,
          null,
        ],
        red: [
          null,
          null,
          null,
          null,
          null,
        ],
      },
      bans: {
        blue: [
          null,
          null,
          null,
          null,
          null,
        ],
        red: [
          null,
          null,
          null,
          null,
          null,
        ],
      },
    };

    this.fetchChampionScoreData();
    this.fetchChampionRoles();
  }

  fetchChampionScoreData() {
    let state = this.state;
    const url = serverUrl + "/get-counter-data";
    fetch(url)
        .then(response => response.json())
        .then(
            (championScoreData) => {
              state.championScoreData = championScoreData;
              this.updateChampionScores(state);
            },
            (error) => {
              console.log("Error trying to retrieve champion score data:");
              console.log(error);
              this.setState(state);
            }
        );
  }

  fetchChampionRoles() {
    let state = this.state;
    const url = serverUrl + "/get-roles";
    fetch(url)
        .then(response => response.json())
        .then(
            (championRoles) => {
              state.championRoles = championRoles;
              this.setState(state);
            },
            (error) => {
              console.log("Error trying to retrieve champion roles:");
              console.log(error);
              this.setState(state);
            }
        );
  }

  updateChampionScores(state) {
    if (!state) {
      state = this.state;
    }

    const currentState = draftStates[this.getDraftOrder(state.draftType)[state.currentStep]];
    let champions = [];
    if ((currentState.phase === "BAN" && currentState.activeTeam === "blue")
        || (currentState.phase === "PICK" && currentState.activeTeam === "red")) {
      state.picks.blue.forEach(
          (champion) => {
            if (champion && champion.toLowerCase() !== "none") {
              champions.push(champion.toLowerCase());
            }
          }
      );
    } else if ((currentState.phase === "BAN" && currentState.activeTeam === "red")
        || (currentState.phase === "PICK" && currentState.activeTeam === "blue")) {
      state.picks.red.forEach(
        (champion) => {
          if (champion && champion.toLowerCase() !== "none") {
            champions.push(champion.toLowerCase());
          }
        }
      );
    }

    let championScores = {};
    Object.keys(state.championScoreData).forEach(
      (champion) => {
        championScores[champion] = state.championScoreData[champion].base;
      }
    );

    champions.forEach(
      (champion) => {
        state.championScoreData[champion].best.forEach(
          (bestMatchup) => {
            championScores[bestMatchup.id] *= bestMatchup.modifier;
          }
        );
        state.championScoreData[champion].worst.forEach(
          (worstMatchup) => {
            championScores[worstMatchup.id] *= worstMatchup.modifier;
          }
        );
      }
    );

    state.championScores = championScores;
    this.setState(state);
  }

  getDraftOrder(draftType) {
    if (!draftType) {
      draftType = this.state.draftType;
    }

    if (draftType === "NORMAL") {
      return normalDraftOrder;
    } else if (draftType === "TOURNAMENT") {
      return tournamentDraftOrder;
    }
  }

  getDraftStep(step) {
    if (!step) {
      step = this.state.currentStep;
    }

    return this.getDraftOrder()[step];
  }

  reset() {
    let state = this.state;
    state.currentStep = 0;
    state.activeHover = null;
    state.picks.blue = [null, null, null, null, null];
    state.picks.red = [null, null, null, null, null];
    state.bans.blue = [null, null, null, null, null];
    state.bans.red = [null, null, null, null, null];
    this.updateChampionScores(state);
  }

  backward() {
    let state = this.state;
    if (state.currentStep > 0) {
      state.currentStep -= 1;
    }

    // TODO: De-dup this code with the selectChampion() method.
    const currentState = draftStates[this.getDraftStep(state.currentStep)];
    const phase = currentState.phase;
    if (phase === "BAN") {
      state.bans[currentState.activeTeam][currentState.activePlayer - 1] = null;
    } else if (phase === "PICK") {
      state.picks[currentState.activeTeam][currentState.activePlayer - 1] = null;
    }

    state.activeHover = null;
    this.updateChampionScores(state);
  }

  selectChampion(championName) {
    const currentState = draftStates[this.getDraftStep()];
    const phase = currentState.phase;
    if (phase === "complete") {
      return;
    }

    let state = this.state;
    if (phase === "BAN") {
      state.bans[currentState.activeTeam][currentState.activePlayer - 1] = championName;
    } else if (phase === "PICK") {
      state.picks[currentState.activeTeam][currentState.activePlayer - 1] = championName;
    }

    if (state.currentStep < this.getDraftOrder(state.draftType).length - 1) {
      state.currentStep += 1;
    }

    state.activeHover = null;
    this.updateChampionScores(state);
  }

  setActiveHover(championName) {
    let state = this.state;
    state.activeHover = championName;
    this.setState(state);
  }

  render() {
    const currentState = draftStates[this.getDraftStep()];
    const nextState = (this.state.currentStep < this.getDraftOrder().length - 1)
        ? draftStates[this.getDraftStep(this.state.currentStep + 1)]
        : null;

    return (
      <div className="draft">
        <PlayerBlock
          team="blue"
          currentState={currentState}
          nextState={nextState}
          picks={this.state.picks}
          activeHover={this.state.activeHover}
          draftType={this.state.draftType}
        />
        <div className="center-draft-block">
          <InfoBlock
              status={draftStates[this.getDraftStep()]["status"]}
              activeTeam={draftStates[this.getDraftStep()]["activeTeam"]}
              onUndoButtonClick={() => this.backward()}
              onResetButtonClick={() => this.reset()}
          />
          <ChampionBlock
              currentState={currentState}
              bans={this.state.bans}
              picks={this.state.picks}
              championRoles={this.state.championRoles}
              championScores={this.state.championScores}
              draftType={this.state.draftType}
              onChampionMouseEnter={(championName) => this.setActiveHover(championName)}
              onChampionClick={(championName) => this.selectChampion(championName)}
          />
          <div className="center-draft-block-bans">
            <BanBlock
                team="blue"
                currentState={currentState}
                bans={this.state.bans}
                activeHover={this.state.activeHover}
            />
            <BanBlock
                team="red"
                currentState={currentState}
                bans={this.state.bans}
                activeHover={this.state.activeHover}
            />
          </div>
        </div>
        <PlayerBlock
          team="red"
          currentState={currentState}
          nextState={nextState}
          picks={this.state.picks}
          activeHover={this.state.activeHover}
          draftType={this.state.draftType}
        />
      </div>
    );
  }
}

export default Draft;
