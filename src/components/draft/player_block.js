import React, {Component} from 'react';

import 'styles/draft/player_block.css';

import LightBar from 'components/draft/light_bar.js';
import Champion from 'components/draft/champion.js';

class Player extends Component {

  render() {
    const championImage = (
      <Champion
          key={"champion"}
          championName={this.props.playerPick}
          type="cropped_splash"
          team={this.props.team}
      />
    );

    let verticalElements = [];
    verticalElements.push(championImage);

    if (this.props.isActivePlayer) {
      let phaseOverlayDescription = "Banning";
      if (this.props.phase === "PICK") {
        phaseOverlayDescription = "Picking";
      }

      verticalElements.push(
        <div key={"phase-overlay"} className="phase-overlay">
            <div className="description">{phaseOverlayDescription}</div>
        </div>
      );
    }

    const playerVertical = (
      <div key={"player-vertical"} className="player-vertical">{verticalElements}</div>
    );
    const lightBar = (
      <LightBar
          key={"light-bar"}
          team={this.props.team}
          isActivePlayer={this.props.isActivePlayer}
          isNextPlayer={this.props.isNextPlayer}
          isActiveTeam={this.props.isActiveTeam}
      />
    );

    let horizontalElements = [];
    if (this.props.team === "blue") {
      horizontalElements.push(playerVertical);
      horizontalElements.push(lightBar);
    } else {
      horizontalElements.push(lightBar);
      horizontalElements.push(playerVertical);
    }

    return (
      <div
          className={
              "player " + this.props.team
                  + (this.props.isActivePlayer ? " active-player" : "")
                  + (this.props.isNextPlayer ? " next-player" : "")}
      >
        {horizontalElements}
      </div>
    );
  }
}

class PlayerBlock extends Component {

  render() {
    const players = [1, 2, 3, 4, 5].map(
      (playerNum) => {
        const isActiveTeam = this.props.team === this.props.currentState.activeTeam;

        let isActivePlayer = false;
        if (isActiveTeam) {
          if (this.props.currentState.phase === "BAN" && this.props.draftType === "TOURNAMENT") {
            isActivePlayer = playerNum === 1;
          } else {
            isActivePlayer = playerNum === this.props.currentState.activePlayer;
          }
        }

        let isNextPlayer = false;
        if (this.props.nextState && this.props.nextState.phase !== "complete") {
          if (this.props.team === this.props.nextState.activeTeam) {
            if (this.props.nextState.phase === "BAN" && this.props.draftType === "TOURNAMENT") {
              isNextPlayer = playerNum === 1;
            } else {
              isNextPlayer = playerNum === this.props.nextState.activePlayer;
            }
          }
        }

        let playerPick = this.props.picks[this.props.team][playerNum - 1];
        if (isActivePlayer && this.props.currentState.phase === "PICK" && this.props.activeHover) {
          playerPick = this.props.activeHover;
        }

        return (
          <Player
              key={this.props.team + "-" + playerNum}
              phase={this.props.currentState.phase}
              team={this.props.team}
              isActiveTeam={isActiveTeam}
              isActivePlayer={isActivePlayer}
              isNextPlayer={isNextPlayer}
              playerPick={playerPick}
          />
        );
      }
    );

    let playerBlockStyle = "";
    if (this.props.team === this.props.currentState.activeTeam) {
      playerBlockStyle = " active-team";
    } else if (this.props.currentState.phase === "complete") {
      playerBlockStyle = " complete";
    }

    return (
      <div className={"player-block" + playerBlockStyle}>
        {players}
      </div>
    );
  }
}

export default PlayerBlock;
