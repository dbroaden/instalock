import React, {Component} from 'react';

import 'styles/draft/light_bar.css';

class LightBar extends Component {

  render() {
    return (
      <div
          className={
              "light-bar " + this.props.team + (this.props.isActivePlayer ? " active-player" : "")
                  + (this.props.isNextPlayer ? " next-player" : "")
                  + (this.props.isActiveTeam ? " active-team" : "")}
      >
      </div>
    );
  }
}

export default LightBar;
