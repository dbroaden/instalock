import React, {Component} from 'react';

import 'styles/draft/info_block.css';

import images from 'assets/images.js';

class UndoButton extends Component {

  render() {
    let img_src = "undo_button.png";
    if (this.props.activeTeam === "blue") {
      img_src = "undo_button_blue.png";
    } else if (this.props.activeTeam === "red") {
      img_src = "undo_button_red.png";
    }

    return (
      <div className="undo-button" onClick={this.props.onUndoButtonClick}>
        <img src={images[img_src]} alt="undo last action" />
      </div>
    );
  }
}

class ResetButton extends Component {

  render() {
    let img_src = "reset_button.png";
    if (this.props.activeTeam === "blue") {
      img_src = "reset_button_blue.png";
    } else if (this.props.activeTeam === "red") {
      img_src = "reset_button_red.png";
    }

    return (
      <div className="reset-button" onClick={this.props.onResetButtonClick}>
        <img src={images[img_src]} alt="reset the draft" />
      </div>
    );
  }
}

class InfoBlock extends Component {

  render() {
    let activeTeamStyle = "";
    if (this.props.activeTeam) {
      activeTeamStyle = " " + this.props.activeTeam;
    }

    return (
      <div className={"info-block" + activeTeamStyle}>
        <div className="status">
          {this.props.status}
        </div>
        <div className="info-block-controls">
          <UndoButton
              onUndoButtonClick={this.props.onUndoButtonClick}
              activeTeam={this.props.activeTeam}
          />
          <ResetButton
              onResetButtonClick={this.props.onResetButtonClick}
              activeTeam={this.props.activeTeam}
          />
        </div>
      </div>
    );
  }
}

export default InfoBlock;
