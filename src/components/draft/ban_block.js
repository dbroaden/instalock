import React, {Component} from 'react';

import 'styles/draft/ban_block.css';

import Champion from 'components/draft/champion.js';

class Ban extends Component {

  render() {
    return (
      <div className={"ban" + (this.props.isActivePlayer ? " active-player" : "")}>
        <div className="ban-overlay-container">
          <Champion
              championName={this.props.championName}
              type="cropped_splash"
              team={this.props.team}
          />
          {this.props.isActivePlayer
              ? <div key={"ban-overlay"} className="ban-overlay"></div>
              : null}
        </div>
      </div>
    );
  }
}

class BanBlock extends Component {

  render() {
    const isActiveTeam =
        this.props.currentState.phase === "BAN"
            && this.props.team === this.props.currentState.activeTeam;

    let bans = [1, 2, 3, 4, 5].map(
      (banNum) => {
        const isActivePlayer = isActiveTeam && banNum === this.props.currentState.activePlayer;
        let playerBan = this.props.bans[this.props.team][banNum - 1];
        if (isActivePlayer && this.props.activeHover) {
          playerBan = this.props.activeHover;
        }

        return (
          <Ban
              key={this.props.team + "-" + banNum}
              championName={playerBan}
              team={this.props.team}
              isActivePlayer={isActivePlayer}
          />
        );
      }
    );
    if (this.props.team === "red") {
      bans = bans.reverse();
    }

    return (
      <div className="ban-block">
        <div
            className={"ban-block-title " + this.props.team + (isActiveTeam ? " active-team" : "")}
        >
          Bans
        </div>
        <div className={"ban-block-list " + this.props.team}>
          {bans}
        </div>
      </div>
    );
  }
}

export default BanBlock;
