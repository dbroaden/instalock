import React, {Component} from 'react';

import 'styles/draft/champion_block.css';

import Champion, {allChampions} from 'components/draft/champion.js';

class SortText extends Component {

  render() {
    return (
      <div className="sort-text">
        Sort:
      </div>
    );
  }
}

class SortButtons extends Component {

  createActiveButton(sortMode, text) {
    return (
      <input
          key={sortMode}
          type="button"
          value={text}
          onClick={() => this.props.onSortModeClick(sortMode)}
      />
    );
  }

  createDisabledButton(sortMode, text) {
    return <input key={sortMode} className="disabled" type="button" value={text} />;
  }

  render() {
    const sortButtons = [["SCORE", "Score"], ["ALPHABETICAL", "A-Z"]].map(
      ([sortMode, text]) => {
        if (sortMode === this.props.sortMode) {
          return this.createDisabledButton(sortMode, text);
        }
        return this.createActiveButton(sortMode, text);
      }
    );

    return (
      <div className="sort-buttons">
        {sortButtons}
      </div>
    );
  }
}

class RoleButtons extends Component {

  createActiveButton(filterRole, text) {
    return (
      <input
          key={filterRole}
          type="button"
          value={text}
          onClick={() => this.props.onFilterRoleClick(filterRole)}
      />
    );
  }

  createDisabledButton(filterRole, text) {
    return <input key={filterRole} className="disabled" type="button" value={text} />;
  }

  render() {
    const roleButtons = [
        ["ALL", "All"],
        ["TOP", "Top"],
        ["JNG", "Jng"],
        ["MID", "Mid"],
        ["BOT", "Bot"],
        ["SUP", "Sup"]].map(
      ([filterRole, text]) => {
        if (filterRole === this.props.filterRole) {
          return this.createDisabledButton(filterRole, text);
        }
        return this.createActiveButton(filterRole, text);
      }
    );

    return (
      <div className="role-buttons">
        {roleButtons}
      </div>
    );
  }
}

class SearchBar extends Component {

  render() {
    return (
      <div className="search-bar">
        <input
            type="search"
            placeholder="Filter..."
            value={this.props.filterQuery}
            onChange={(event) => this.props.onFilterQueryChange(event.target.value)}
        >
        </input>
      </div>
    );
  }
}

class ChampionControls extends Component {

  render() {
    return (
      <div
          className={
              "champion-controls" + (this.props.activeTeam ? " " + this.props.activeTeam : "")}
      >
        <SortText />
        <SortButtons sortMode={this.props.sortMode} onSortModeClick={this.props.onSortModeClick} />
        <RoleButtons
            filterRole={this.props.filterRole}
            onFilterRoleClick={this.props.onFilterRoleClick}
        />
        <SearchBar
            onFilterQueryChange={this.props.onFilterQueryChange}
            filterQuery={this.props.filterQuery}
        />
      </div>
    );
  }
}

class ChampionName extends Component {

  render() {
    return (
      <div className="champion-name">
        {this.props.championName}
      </div>
    );
  }
}

class ChampionSelection extends Component {

  render() {
    return (
      <div
          className="champion-selection"
          onMouseEnter={() => this.props.onChampionMouseEnter(this.props.championName)}
          onClick={() => this.props.onChampionClick(this.props.championName)}
      >
        <Champion championName={this.props.championName} type="icon" />
        <ChampionName championName={this.props.championName} />
      </div>
    );
  }
}

class ChampionList extends Component {

  render() {
    let champions = this.props.champions.map(
      (champion) =>
        <ChampionSelection
            key={champion}
            championName={champion}
            onChampionMouseEnter={this.props.onChampionMouseEnter}
            onChampionClick={this.props.onChampionClick}
        />
    );

    // Append "None" champion option to front of the selection list.
    champions.unshift(
      <ChampionSelection
          key="None"
          championName="None"
          onChampionMouseEnter={this.props.onChampionMouseEnter}
          onChampionClick={this.props.onChampionClick}
      />
    );

    // These are required for left-aligning the last row of the list.
    let hiddenSpacers = [];
    for (let i = 0; i < 25; i++) {
      hiddenSpacers.push(<div key={"hidden-spacer-" + i} className="hidden-spacer" />)
    };

    return (
      <div className={"champion-list" + (this.props.activeTeam ? " " + this.props.activeTeam : "")}>
        {champions}
        {hiddenSpacers}
      </div>
    );
  }
}

class ChampionBlock extends Component {

  constructor(props) {
    super(props);
    this.state = {
      sortMode: "ALPHABETICAL",
      filterRole: "ALL",
      filterQuery: "",
    };
  }

  getFilteredChampionList() {
    return allChampions.filter(
        (champion) => this.matchesFilterQuery(champion)
            && this.matchesFilterRole(champion)
            && this.isAvailable(champion));
  }

  matchesFilterRole(champion) {
    if (this.state.filterRole === "ALL" || !this.props.championRoles) {
      return true;
    }

    champion = champion.toLowerCase();
    if (!(champion in this.props.championRoles)) {
      return false;
    }

    return this.props.championRoles[champion].includes(this.state.filterRole.toLowerCase());
  }

  matchesFilterQuery(champion) {
    if (!this.state.filterQuery) {
      return true;
    }

    return champion.toLowerCase().includes(this.state.filterQuery.toLowerCase());
  }

  isAvailable(champion) {
    if (this.props.picks.blue.includes(champion) || this.props.picks.red.includes(champion)) {
      return false;
    }

    const isBlueBan = this.props.bans.blue.includes(champion);
    const isRedBan = this.props.bans.red.includes(champion);
    if (this.props.currentState.phase === "BAN" && this.props.draftType === "NORMAL") {
      return this.props.currentState.activeTeam === "blue" ? !isBlueBan : !isRedBan;
    }

    return !isBlueBan && !isRedBan;
  }

  getSortedChampionList() {
    let filteredChampions = this.getFilteredChampionList();
    if (this.state.sortMode === "SCORE") {
      filteredChampions.sort((a, b) => this.scoreSortComparator(a, b));
    } else if (this.state.sortMode === "ALPHABETICAL") {
      filteredChampions.sort((a, b) => this.alphaSortComparator(a, b));
    }

    return filteredChampions;
  }

  getChampionScore(champion) {
    const championKey = champion.toLowerCase();
    if (!this.props.championScores || !(championKey in this.props.championScores)) {
      return 0;
    }

    return this.props.championScores[championKey];
  }

  scoreSortComparator(firstChampion, secondChampion) {
    const firstChampionScore = this.getChampionScore(firstChampion);
    const secondChampionScore = this.getChampionScore(secondChampion);
    if (firstChampionScore > secondChampionScore) {
      return -1;
    } else if (firstChampionScore < secondChampionScore) {
      return 1;
    }

    return firstChampion.localeCompare(secondChampion); 
  }

  alphaSortComparator(firstChampion, secondChampion) {
    return firstChampion.localeCompare(secondChampion); 
  }

  setSortMode(sortMode) {
    let state = this.state;
    state.sortMode = sortMode;
    this.setState(state);
  }

  setFilterRole(filterRole) {
    let state = this.state;
    state.filterRole = filterRole;
    this.setState(state);
  }

  setFilterQuery(filterQuery) {
    let state = this.state;
    state.filterQuery = filterQuery;
    this.setState(state);
  }

  render() {
    return (
      <div className="champion-block">
        <ChampionControls
            activeTeam={this.props.currentState.activeTeam}
            sortMode={this.state.sortMode}
            filterRole={this.state.filterRole}
            filterQuery={this.state.filterQuery}
            onSortModeClick={(sortMode) => this.setSortMode(sortMode)}
            onFilterRoleClick={(filterRole) => this.setFilterRole(filterRole)}
            onFilterQueryChange={(filterQuery) => this.setFilterQuery(filterQuery)}
        />
        <ChampionList
            champions={this.getSortedChampionList()}
            activeTeam={this.props.currentState.activeTeam}
            onChampionMouseEnter={this.props.onChampionMouseEnter}
            onChampionClick={
                (champion) => {
                  this.setFilterRole("ALL");
                  this.setFilterQuery("");
                  this.props.onChampionClick(champion);
                }
            }
        />
      </div>
    );
  }
}

export default ChampionBlock;
