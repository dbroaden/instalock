import React, {Component} from 'react';

import 'styles/banner.css';

class Banner extends Component {

  render() {
    return (
      <div className="banner">
        <h1>Welcome to InstaLock</h1>
      </div>
    );
  }
}

export default Banner;
