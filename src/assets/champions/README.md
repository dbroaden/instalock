# Champion Image Locations

* **icon**: `https://ddragon.leagueoflegends.com/cdn/13.8.1/img/champion/<Champion>.png`
* **splash**: `https://ddragon.leagueoflegends.com/cdn/img/champion/splash/<Champion>_0.jpg`
