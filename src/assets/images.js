/* Statically imports all image assets for use by components. */

function importAll(r) {
    return r.keys().reduce(
        function(output, key) {
            output[key.replace('./', '')] = r(key);
            return output;
        },
        {});
}

const images = importAll(require.context('assets', true, /\.(png|jpe?g|svg)$/));

export default images;
