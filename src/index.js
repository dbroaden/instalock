import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {HashRouter, Route} from 'react-router-dom';

import AutoDraftPage from 'components/autodraft_page.js'
import DraftPage from 'components/draft_page.js'

class SiteRouter extends Component {

  render() {
    return (
      <HashRouter>
        <Route exact path="/" component={DraftPage} />
        <Route path="/autodraft" component={AutoDraftPage} />
      </HashRouter>
    );
  }
}

ReactDOM.render(
  <SiteRouter />,
  document.getElementById('root')
);
