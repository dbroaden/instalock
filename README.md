This project was bootstrapped with [Create React App][1].

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode. Open [http://localhost:3000][2] to view it
in the browser.

The page will reload if you make edits. You will also see any lint errors in the
console.

### `yarn test`

Launches the test runner in the interactive watch mode. See the section about
[running tests][3] for more information.

### `yarn build`

Builds the app for production to the `build` folder. It correctly bundles React
in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes. Your app is ready to
be deployed!

See the section about [deployment][4] for more information.



[1]: https://github.com/facebook/create-react-app
[2]: http://localhost:3000
[3]: https://facebook.github.io/create-react-app/docs/running-tests
[4]: https://facebook.github.io/create-react-app/docs/deployment
