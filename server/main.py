import flask
import json
import re


# If `entrypoint` is not defined in app.yaml, App Engine will look for an app
# called `app` in `main.py`.
app = flask.Flask(__name__)


VALID_CORS_DOMAINS = [
  'instalock.appspot.com',
  'instalock.wl.r.appspot.com',
]
MATCHUP_DATA_FILE_PATH = 'data/champion_matchup_data.json'
SCORE_DATA_FILE_PATH = 'data/champion_score_data.json'
MATCHUP_DATA = dict()
SCORE_DATA = dict()
BASE_SCORES = dict()
CHAMPION_ROLES = dict()


@app.route('/get-counter-data', methods=['GET'])
def getCounterData():
  request_origin = _validateOrigin()

  response = flask.Response(json.dumps(_getScoreData(), sort_keys=True))

  # Grant CORS access
  response.headers['Access-Control-Allow-Origin'] = request_origin
  return response


@app.route('/get-roles', methods=['GET'])
def getRoles():
  global CHAMPION_ROLES

  request_origin = _validateOrigin()

  if not CHAMPION_ROLES:
    for champion, data in _getMatchupData().items():
      CHAMPION_ROLES[champion] = data['roles']

  response = flask.Response(json.dumps(CHAMPION_ROLES, sort_keys=True))

  # Grant CORS access
  response.headers['Access-Control-Allow-Origin'] = request_origin
  return response


class ForbiddenAccessException(Exception):

  def __init__(self):
    Exception.__init__(self)
    self.message = 'Access is forbidden'
    self.status_code = 403


@app.errorhandler(ForbiddenAccessException)
def handleForbiddenAccess(error):
  response = flask.jsonify({
    'message': '{}: {}'.format(error.status_code, error.message),
  })
  response.status_code = error.status_code
  return response


def _validateOrigin():
  if 'HTTP_ORIGIN' in flask.request.environ:
    request_origin = flask.request.environ['HTTP_ORIGIN']
    origin_domain = re.sub(r'http(s)?://(www\.)?', '', request_origin)
    if origin_domain in VALID_CORS_DOMAINS:
      return request_origin

  raise ForbiddenAccessException


def _getMatchupData():
  global MATCHUP_DATA

  if MATCHUP_DATA:
    return MATCHUP_DATA

  with open(MATCHUP_DATA_FILE_PATH, 'r') as matchup_data_file:
    MATCHUP_DATA = json.load(matchup_data_file)

  return MATCHUP_DATA


def _getScoreData():
  global SCORE_DATA

  if SCORE_DATA:
    return SCORE_DATA

  with open(SCORE_DATA_FILE_PATH, 'r') as score_data_file:
    SCORE_DATA = json.load(score_data_file)

  return SCORE_DATA


if __name__ == '__main__':
    # This is used when running locally only. When deploying to Google App
    # Engine, a webserver process such as Gunicorn will serve the app. This
    # can be configured by adding an `entrypoint` to app.yaml.
    app.run(host='127.0.0.1', port=8080, debug=True)
