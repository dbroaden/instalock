#! /usr/bin/env python3

import json
import random
import re
import requests
import sys
import time
import urllib.request

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager

ALL_CHAMPIONS = [
  'aatrox',
  'ahri',
  'akali',
  'akshan',
  'alistar',
  'amumu',
  'anivia',
  'annie',
  'aphelios',
  'ashe',
  'aurelionsol',
  'azir',
  'bard',
  'belveth',
  'blitzcrank',
  'brand',
  'braum',
  'caitlyn',
  'camille',
  'cassiopeia',
  'chogath',
  'corki',
  'darius',
  'diana',
  'draven',
  'drmundo',
  'ekko',
  'elise',
  'evelynn',
  'ezreal',
  'fiddlesticks',
  'fiora',
  'fizz',
  'galio',
  'gangplank',
  'garen',
  'gnar',
  'gragas',
  'graves',
  'gwen',
  'hecarim',
  'heimerdinger',
  'illaoi',
  'irelia',
  'ivern',
  'janna',
  'jarvaniv',
  'jax',
  'jayce',
  'jhin',
  'jinx',
  'kaisa',
  'kalista',
  'karma',
  'karthus',
  'kassadin',
  'katarina',
  'kayle',
  'kayn',
  'kennen',
  'khazix',
  'kindred',
  'kled',
  'kogmaw',
  'ksante',
  'leblanc',
  'leesin',
  'leona',
  'lillia',
  'lissandra',
  'lucian',
  'lulu',
  'lux',
  'malphite',
  'malzahar',
  'maokai',
  'masteryi',
  'milio',
  'missfortune',
  'mordekaiser',
  'morgana',
  'nami',
  'nasus',
  'nautilus',
  'neeko',
  'nidalee',
  'nilah',
  'nocturne',
  'nunu',
  'olaf',
  'orianna',
  'ornn',
  'pantheon',
  'poppy',
  'pyke',
  'qiyana',
  'quinn',
  'rakan',
  'rammus',
  'reksai',
  'rell',
  'renata',
  'renekton',
  'rengar',
  'riven',
  'rumble',
  'ryze',
  'samira',
  'sejuani',
  'senna',
  'seraphine',
  'sett',
  'shaco',
  'shen',
  'shyvana',
  'singed',
  'sion',
  'sivir',
  'skarner',
  'sona',
  'soraka',
  'swain',
  'sylas',
  'syndra',
  'tahmkench',
  'taliyah',
  'talon',
  'taric',
  'teemo',
  'thresh',
  'tristana',
  'trundle',
  'tryndamere',
  'twistedfate',
  'twitch',
  'udyr',
  'urgot',
  'varus',
  'vayne',
  'veigar',
  'velkoz',
  'vex',
  'vi',
  'viego',
  'viktor',
  'vladimir',
  'volibear',
  'warwick',
  'wukong',
  'xayah',
  'xerath',
  'xinzhao',
  'yasuo',
  'yone',
  'yorick',
  'yuumi',
  'zac',
  'zed',
  'zeri',
  'ziggs',
  'zilean',
  'zoe',
  'zyra',
]
MATCHUP_DATA_FILE_PATH = '../server/data/champion_matchup_data.new.json'

TIER_LIST_URL = 'https://u.gg/lol/tier-list'
BUILD_URL_TEMPLATE = 'https://u.gg/lol/champions/{}/build'
COUNTER_URL_TEMPLATE = 'https://u.gg/lol/champions/{}/counter'

PAGE_LOAD_WAIT_SECONDS = 2
TARGET_RATE_LIMIT_SECONDS = 12
SCROLL_WAIT_SECONDS = 2

NON_NUMBER_REGEX = re.compile('[^0-9.]')


def main(argv):
  start_time = time.time()
  random.seed()

  print(
      'Preparing to scrape matchups for {} champions'.format(
          len(ALL_CHAMPIONS)))

  with WebDriver() as driver:
    champion_roles = dict()
    tier_list_tags = BeautifulSoup(
        driver.get(TIER_LIST_URL, scroll_full_page=True),
        'html.parser')
    champion_rows = tier_list_tags.find_all('div', 'rt-tr-group')
    for champion_row in champion_rows:
      role = parseRoleFromTierList(
          champion_row.find('img', 'tier-list-role')['alt'])
      if not role:
        continue

      champion = champion_row.find('strong', 'champion-name').string.lower()
      if champion in champion_roles:
        if not role in champion_roles[champion]:
          champion_roles[champion].append(role)
      else:
        champion_roles[champion] = [role]

    print('Fetched roles for {} champions'.format(len(champion_roles)))

    matchup_data = dict()
    for i, champion in enumerate(ALL_CHAMPIONS):
      # Overall
      build_tags = BeautifulSoup(
          driver.get(BUILD_URL_TEMPLATE.format(champion)),
          'html.parser')
      overall_win = parseNumStr(
          build_tags.find('div', 'win-rate').find('div', 'value').string)
      overall_pick = parseNumStr(
          build_tags.find('div', 'pick-rate').find('div', 'value').string)
      overall_ban = parseNumStr(
          build_tags.find('div', 'ban-rate').find('div', 'value').string)

      # Roles
      roles = []
      if champion in champion_roles:
        roles = champion_roles[champion]

      main_role = parseRoleFromChampionTitle(
          build_tags.find('span', 'champion-title').string)
      if main_role and not main_role in roles:
        roles.append(main_role)

      # Matchups
      counter_tags = BeautifulSoup(
          driver.get(COUNTER_URL_TEMPLATE.format(champion)),
          'html.parser')

      best_matchups = []
      best_tags = counter_tags.find_all('a', 'counter-list-card worst-win-rate')
      for matchup in best_tags:
        champion_id = (
            matchup['href'].replace('/lol/champions/', '').replace('/build', ''))
        matchup_win = (100 - parseNumStr(matchup.find('div', 'win-rate').string))
        best_matchups.append({'id': champion_id, 'win': matchup_win})

      worst_matchups = []
      worst_tags = counter_tags.find_all('a', 'counter-list-card best-win-rate')
      for matchup in worst_tags:
        champion_id = (
            matchup['href'].replace('/lol/champions/', '').replace('/build', ''))
        matchup_win = (100 - parseNumStr(matchup.find('div', 'win-rate').string))
        worst_matchups.append({'id': champion_id, 'win': matchup_win})

      # Add data
      matchup_data[champion] = {
        'win': overall_win,
        'pick': overall_pick,
        'ban': overall_ban,
        'roles': roles,
        'best': best_matchups,
        'worst': worst_matchups,
      }

      current_time = time.time()
      print(
          'Processed {}/{} ({}s)'.format(
              i + 1,
              len(ALL_CHAMPIONS),
              int(current_time - start_time)))

  # Save to file
  with open(MATCHUP_DATA_FILE_PATH, 'w') as matchup_data_file:
    json.dump(matchup_data, matchup_data_file, indent=4, sort_keys=True)


  print(
      'Matchups successfully scraped and saved to {}'.format(
          MATCHUP_DATA_FILE_PATH))


class WebDriver:

  def __init__(
      self,
      page_load_wait_seconds=PAGE_LOAD_WAIT_SECONDS,
      target_rate_limit_seconds=TARGET_RATE_LIMIT_SECONDS,
      scroll_wait_seconds=SCROLL_WAIT_SECONDS):
    self.page_load_wait_seconds = page_load_wait_seconds
    self.target_rate_limit_seconds = target_rate_limit_seconds
    self.scroll_wait_seconds = scroll_wait_seconds
    self.driver = None

    self.initDriver()

  def __enter__(self):
    self.initDriver()
    return self

  def __exit__(self, exception_type, exception_value, traceback):
    self.closeDriver()

  def _waitForPageLoad(self):
    time.sleep(self.page_load_wait_seconds)

  def _throttleWait(self):
    wait_duration = (
        (self.target_rate_limit_seconds / 2)
            + random.randint(0, self.target_rate_limit_seconds))
    time.sleep(wait_duration)

  def _scrollFullPage(self):
    self._waitForPageLoad()
    prev_height = None
    curr_height = self.driver.execute_script(
        'return document.body.scrollHeight')

    # Scroll until the bottom of the page is reached
    while curr_height != prev_height:
        prev_height = curr_height
        self.driver.execute_script(
            'window.scrollTo(0, document.body.scrollHeight);')
        time.sleep(self.scroll_wait_seconds)
        curr_height = self.driver.execute_script(
            'return document.body.scrollHeight')

  def get(self, url, scroll_full_page=False):
    self.driver.get(url)

    if scroll_full_page:
      self._scrollFullPage()

    self._waitForPageLoad()
    return self.driver.page_source

  def initDriver(self):
    if self.driver:
      return

    options = Options()
    options.headless = True
    options.add_argument('window-size=1920,1080')
    self.driver = webdriver.Chrome(
        service=Service(ChromeDriverManager().install()),
        options=options)

  def closeDriver(self):
    if not self.driver:
      return

    self.driver.quit()
    self.driver = None


def parseNumStr(orig_str):
  stripped_str = NON_NUMBER_REGEX.sub('', orig_str)
  if not stripped_str:
    return 0.0

  return float(stripped_str)


def parseRoleFromTierList(role):
  role = role.lower()
  if role == 'top':
    return 'top'
  elif role == 'jungle':
    return 'jng'
  elif role == 'mid':
    return 'mid'
  elif role == 'adc':
    return 'bot'
  elif role == 'supp':
    return 'sup'

  print('Unknown role in tier list: {}'.format(role))
  return None


def parseRoleFromChampionTitle(title):
  title = title.lower()
  if not 'build for ' in title or not ',' in title:
    return None

  comma_index = title.find(',')
  role = title[10:comma_index]
  if role == 'top':
    return 'top'
  elif role == 'jungle':
    return 'jng'
  elif role == 'mid':
    return 'mid'
  elif role == 'adc':
    return 'bot'
  elif role == 'support':
    return 'sup'

  print('Unknown role in champion title: {}'.format(role))
  return None

if __name__ == '__main__':
  main(sys.argv[1:])
