#! /usr/bin/env python3

import json
import sys


MATCHUP_DATA_FILE_PATH = "../server/data/champion_matchup_data.json"
SCORE_DATA_FILE_PATH = "../server/data/champion_score_data.new.json"
MAX_POPULARITY_MODIFIER = 0.1
MAX_MATCHUP_MODIFIER = 0.2


def getPopularity(champion_matchup_data):
  return champion_matchup_data['pick'] + champion_matchup_data['ban']


def main(argv):
  # Retrieve matchup data
  matchup_data = dict()
  with open(MATCHUP_DATA_FILE_PATH, "r") as matchup_data_file:
    matchup_data = json.load(matchup_data_file)

  popularities = [getPopularity(v) for v in matchup_data.values()]
  min_popularity = min(popularities)
  max_popularity = max(popularities)

  score_data = dict()
  for champion, champion_data in matchup_data.items():
    popularity_modifier = (
        (1 - MAX_POPULARITY_MODIFIER)
            + ((getPopularity(champion_data) - min_popularity)
                / ((max_popularity - min_popularity) * 1.0))
            * (MAX_POPULARITY_MODIFIER * 2))
    base_score = champion_data['win'] * popularity_modifier

    best_matchup_rate = max(
        [matchup['win'] for matchup in champion_data['best']])
    worst_matchup_rate = min(
        [matchup['win'] for matchup in champion_data['worst']])

    best_modifiers = []
    for matchup in champion_data['best']:
      base_modifier = (
          (matchup['win'] - worst_matchup_rate)
              / ((best_matchup_rate - worst_matchup_rate) * 1.0))
      if base_modifier < 0:
        base_modifier = 0

      modifier = 1 - (base_modifier * MAX_MATCHUP_MODIFIER)
      best_modifiers.append({'id': matchup['id'], 'modifier': modifier})

    worst_modifiers = []
    for matchup in champion_data['worst']:
      base_modifier = (
          (matchup['win'] - best_matchup_rate)
              / ((worst_matchup_rate - best_matchup_rate) * 1.0))
      if base_modifier < 0:
        base_modifier = 0

      modifier = 1 + (base_modifier * MAX_MATCHUP_MODIFIER)
      worst_modifiers.append({'id': matchup['id'], 'modifier': modifier})

    score_data[champion] = {
      'base': base_score,
      'best': best_modifiers,
      'worst': worst_modifiers,
    }

  # Save to file
  with open(SCORE_DATA_FILE_PATH, "w") as score_data_file:
    json.dump(score_data, score_data_file, indent=4, sort_keys=True)


if __name__ == "__main__":
  main(sys.argv[1:])
